//  ============================================================================================================
//  Dependencies
//  ============================================================================================================

import gulp from "gulp";
import sass from "gulp-sass";
import rename from "gulp-rename";
import sourcemaps from "gulp-sourcemaps";
import cssnano from 'gulp-cssnano';
import autoprefixer from 'gulp-autoprefixer';
import browserSync  from 'browser-sync';

// Used for grabbing the module name of an external library rather than the file path
import importer from "sass-module-importer"

// Path of our sass file
const sassPath = 'a-src/scss/**/*.scss';

// This is where the minified css will be 
const cssPath  = 'a-dist/assets/css';

//  ============================================================================================================
//  Gulp Task for creating our main stylesheet
//  ============================================================================================================

gulp.task('sass', function(){

    return gulp.src(sassPath)

    // Initiate sourcemaps
    .pipe(sourcemaps.init())

    // Begin our sass'ing
    .pipe(sass())

    // Autoprefixing for browser support
    .pipe(autoprefixer({
        browsers: ['last 2 versions'],
        cascade: false
    }))

    //Minify our css
    .pipe(cssnano())

    // Rename our minified css file
    .pipe(rename("style.css"))

    .pipe(sourcemaps.write())

    // Output destination of
    .pipe(gulp.dest(cssPath))

    .pipe(browserSync.reload({stream: true})); // prompts a reload after compilation
});
