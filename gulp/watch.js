//  ============================================================================================================
//  Dependencies
//  ============================================================================================================

import gulp from 'gulp';
import browserSync from 'browser-sync'

//  ============================================================================================================
//  Start our localhost
//  ============================================================================================================
gulp.task('browserSync', function() {
    browserSync.create();
    browserSync.init({
        server: {
        baseDir: 'a-dist'
        },
    })
})

gulp.task('watch', function a () {
    gulp.watch("a-src/scss/**/*.scss", gulp.series('sass'), browserSync.reload());
    gulp.watch("a-src/js/**/*.js",  gulp.series('scripts'), browserSync.reload());
});
