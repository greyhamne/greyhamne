/**
 * Prototypes - Basic example of how they search the chain of prototypes
*/


// // A basic object
// var person = {
//     name: "Grahamne",
//     age: 30
// };

// // we are now attaching a method called hello to the root protoype
// Object.prototype.hello = function(){
//     console.log("Hello my ssxx is " + person.name);
// };

// // Our person object does not have a property so 
// // js begins look up the chain of protoypes to see if hello exists
// // It finds the hello method in the root proto runs the method
// person.hello();

// /**
//  * Think of it like this
//  * 1. Call our method person.hello()
//  * 2. Javascipt says ahhh its not on persons's prototype and looks higher up the proto chain
//  * 3. Javascipt keeps going back a level unttil it finds a prototype that has it
//  * 4. Finds hello() on the root in this example and says that will do
//  */